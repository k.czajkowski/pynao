from __future__ import print_function, division
import unittest, numpy as np
from pyscf import gto, scf
from pynao import scf as nao_scf

mol = gto.M(verbose=1, atom='''Mn 0 0 0;''', basis='cc-pvdz', spin=5)
gto_mf_uhf = scf.UHF(mol)
gto_mf_uhf.kernel()
dm = gto_mf_uhf.make_rdm1()
jg,kg = gto_mf_uhf.get_jk()

class KnowValues(unittest.TestCase):

    def test_mn_scf_0085(self):
        from pynao.m_fermi_dirac import fermi_dirac_occupations
        """Spin-resolved case redoing SCF procedure. """
        scf = nao_scf(mf=gto_mf_uhf, gto=mol, verbosity=0)
        self.assertEqual(scf.nspin, 2)
        jn,kn = scf.get_jk()
        for d,dr in zip(jg.shape, jn.shape):
            self.assertEqual(d, dr)
        for d,dr in zip(kg.shape, kn.shape):
            self.assertEqual(d, dr)

        dm_nao = scf.make_rdm1()
        Ehartree = (jn*dm_nao.reshape(scf.nspin,scf.norbs,scf.norbs)).sum()/2.0
        Ex = (kn*dm_nao.reshape(scf.nspin,scf.norbs,scf.norbs)).sum()/2.0
        self.assertAlmostEqual(Ehartree, 248.461304275)
        self.assertAlmostEqual(Ex, 50.9912877484)    
        ne_occ = fermi_dirac_occupations(scf.telec, scf.mo_energy, scf.fermi_energy).sum()
        self.assertAlmostEqual(ne_occ, 25.0)

        ## Do unrestricted Hartree-Fock SCF with numerical atomic orbitals
        e_tot = scf.kernel_scf()
        self.assertEqual(scf.nspin, 2)
        self.assertAlmostEqual(e_tot, -1149.86757123)

        ne_occ = fermi_dirac_occupations(scf.telec, scf.mo_energy, scf.fermi_energy).sum()
        self.assertAlmostEqual(ne_occ, 25.0)

        o,dm = scf.overlap_coo().toarray(), scf.make_rdm1()
        for nes, dms in zip(scf.nelec, dm[0,:,:,:,0]):
          self.assertAlmostEqual((dms*o).sum(), nes, 4)

if __name__ == "__main__":
    unittest.main()
