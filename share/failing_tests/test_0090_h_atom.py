from __future__ import print_function, division
import unittest, sys, numpy as np
from pyscf import gto, scf
from pynao import gw as gw_c

mol = gto.M(verbose=0, atom='''H 0.0 0.0 0.0''', basis='aug-cc-pvdz', spin=1)
gto_mf = scf.RHF(mol)
e_tot = gto_mf.kernel()

class KnowValues(unittest.TestCase):

    def test_0090_h_atom(self):
        """ Spin-resolved case GW procedure. """
        #return
        gw = gw_c(mf=gto_mf, gto=mol, verbosity=2, niter_max_ev=16,
                  kmat_algo='dp_vertex_loops_sm')
        self.assertEqual(gw.nspin, 1)
        gw.kernel_gw()
        np.savetxt('eigvals_g0w0_pyscf_rescf_h_0090.txt', gw.mo_energy_gw[0,:,:].T)

if __name__ == "__main__":
    unittest.main()
