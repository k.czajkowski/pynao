pynao package
=============

Submodules
----------

pynao.ao\_log module
--------------------

.. automodule:: pynao.ao_log
   :members:
   :undoc-members:
   :show-inheritance:

pynao.bse\_iter module
----------------------

.. automodule:: pynao.bse_iter
   :members:
   :undoc-members:
   :show-inheritance:

pynao.chi0\_matvec module
-------------------------

.. automodule:: pynao.chi0_matvec
   :members:
   :undoc-members:
   :show-inheritance:

pynao.coords2sort\_order module
-------------------------------

.. automodule:: pynao.coords2sort_order
   :members:
   :undoc-members:
   :show-inheritance:

pynao.gw module
---------------

.. automodule:: pynao.gw
   :members:
   :undoc-members:
   :show-inheritance:

pynao.gw\_iter module
---------------------

.. automodule:: pynao.gw_iter
   :members:
   :undoc-members:
   :show-inheritance:

pynao.log\_mesh module
----------------------

.. automodule:: pynao.log_mesh
   :members:
   :undoc-members:
   :show-inheritance:

pynao.lsofcsr module
--------------------

.. automodule:: pynao.lsofcsr
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_ao\_eval module
------------------------

.. automodule:: pynao.m_ao_eval
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_ao\_eval\_libnao module
--------------------------------

.. automodule:: pynao.m_ao_eval_libnao
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_ao\_log\_hartree module
--------------------------------

.. automodule:: pynao.m_ao_log_hartree
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_ao\_matelem module
---------------------------

.. automodule:: pynao.m_ao_matelem
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_aos\_libnao module
---------------------------

.. automodule:: pynao.m_aos_libnao
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_c2r module
-------------------

.. automodule:: pynao.m_c2r
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_chi0\_noxv module
--------------------------

.. automodule:: pynao.m_chi0_noxv
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_color module
---------------------

.. automodule:: pynao.m_color
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_comp\_coulomb\_den module
----------------------------------

.. automodule:: pynao.m_comp_coulomb_den
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_comp\_coulomb\_pack module
-----------------------------------

.. automodule:: pynao.m_comp_coulomb_pack
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_comp\_dm module
------------------------

.. automodule:: pynao.m_comp_dm
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_comp\_spatial\_distributions module
--------------------------------------------

.. automodule:: pynao.m_comp_spatial_distributions
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_comp\_spatial\_numba module
------------------------------------

.. automodule:: pynao.m_comp_spatial_numba
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_comp\_vext\_tem module
-------------------------------

.. automodule:: pynao.m_comp_vext_tem
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_comp\_vext\_tem\_numba module
--------------------------------------

.. automodule:: pynao.m_comp_vext_tem_numba
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_conv\_ac\_dp module
----------------------------

.. automodule:: pynao.m_conv_ac_dp
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_conv\_yzx2xyz module
-----------------------------

.. automodule:: pynao.m_conv_yzx2xyz
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_coulomb\_am module
---------------------------

.. automodule:: pynao.m_coulomb_am
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_csphar module
----------------------

.. automodule:: pynao.m_csphar
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_csphar\_talman\_libnao module
--------------------------------------

.. automodule:: pynao.m_csphar_talman_libnao
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_dens\_elec\_vec module
-------------------------------

.. automodule:: pynao.m_dens_elec_vec
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_dens\_libnao module
----------------------------

.. automodule:: pynao.m_dens_libnao
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_density\_cart module
-----------------------------

.. automodule:: pynao.m_density_cart
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_dipole\_coo module
---------------------------

.. automodule:: pynao.m_dipole_coo
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_dipole\_ni module
--------------------------

.. automodule:: pynao.m_dipole_ni
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_div\_eigenenergy\_numba module
---------------------------------------

.. automodule:: pynao.m_div_eigenenergy_numba
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_div\_eigenenergy\_numba\_gpu module
--------------------------------------------

.. automodule:: pynao.m_div_eigenenergy_numba_gpu
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_dm module
------------------

.. automodule:: pynao.m_dm
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_dos\_pdos\_eigenvalues module
--------------------------------------

.. automodule:: pynao.m_dos_pdos_eigenvalues
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_dos\_pdos\_ldos module
-------------------------------

.. automodule:: pynao.m_dos_pdos_ldos
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_eri2c module
---------------------

.. automodule:: pynao.m_eri2c
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_eri3c module
---------------------

.. automodule:: pynao.m_eri3c
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_exc module
-------------------

.. automodule:: pynao.m_exc
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_fact module
--------------------

.. automodule:: pynao.m_fact
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_fermi\_dirac module
----------------------------

.. automodule:: pynao.m_fermi_dirac
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_fermi\_energy module
-----------------------------

.. automodule:: pynao.m_fermi_energy
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_fft module
-------------------

.. automodule:: pynao.m_fft
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_fireball\_get\_HS\_dat module
--------------------------------------

.. automodule:: pynao.m_fireball_get_HS_dat
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_fireball\_get\_cdcoeffs\_dat module
--------------------------------------------

.. automodule:: pynao.m_fireball_get_cdcoeffs_dat
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_fireball\_get\_eigen\_dat module
-----------------------------------------

.. automodule:: pynao.m_fireball_get_eigen_dat
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_fireball\_hsx module
-----------------------------

.. automodule:: pynao.m_fireball_hsx
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_fireball\_import module
--------------------------------

.. automodule:: pynao.m_fireball_import
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_g297 module
--------------------

.. automodule:: pynao.m_g297
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_gauleg module
----------------------

.. automodule:: pynao.m_gauleg
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_gaunt module
---------------------

.. automodule:: pynao.m_gaunt
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_get\_atom2bas\_s module
--------------------------------

.. automodule:: pynao.m_get_atom2bas_s
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_get\_sp\_mu2s module
-----------------------------

.. automodule:: pynao.m_get_sp_mu2s
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_gpaw\_hsx module
-------------------------

.. automodule:: pynao.m_gpaw_hsx
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_gpaw\_wfsx module
--------------------------

.. automodule:: pynao.m_gpaw_wfsx
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_gw\_chi0\_noxv module
------------------------------

.. automodule:: pynao.m_gw_chi0_noxv
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_gw\_xvx module
-----------------------

.. automodule:: pynao.m_gw_xvx
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_gw\_xvx\_dp\_sparse module
-----------------------------------

.. automodule:: pynao.m_gw_xvx_dp_sparse
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_init\_dens\_libnao module
----------------------------------

.. automodule:: pynao.m_init_dens_libnao
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_init\_dm\_libnao module
--------------------------------

.. automodule:: pynao.m_init_dm_libnao
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_init\_sab2dm\_libnao module
------------------------------------

.. automodule:: pynao.m_init_sab2dm_libnao
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_ion\_log module
------------------------

.. automodule:: pynao.m_ion_log
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_kernel\_utils module
-----------------------------

.. automodule:: pynao.m_kernel_utils
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_kmat\_den module
-------------------------

.. automodule:: pynao.m_kmat_den
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_laplace\_am module
---------------------------

.. automodule:: pynao.m_laplace_am
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_libnao module
----------------------

.. automodule:: pynao.m_libnao
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_local\_vertex module
-----------------------------

.. automodule:: pynao.m_local_vertex
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_log\_interp module
---------------------------

.. automodule:: pynao.m_log_interp
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_lorentzian module
--------------------------

.. automodule:: pynao.m_lorentzian
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_ls\_contributing module
--------------------------------

.. automodule:: pynao.m_ls_contributing
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_ls\_part\_centers module
---------------------------------

.. automodule:: pynao.m_ls_part_centers
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_next235 module
-----------------------

.. automodule:: pynao.m_next235
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_numba\_utils module
----------------------------

.. automodule:: pynao.m_numba_utils
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_openmx\_import\_scfout module
--------------------------------------

.. automodule:: pynao.m_openmx_import_scfout
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_openmx\_mat module
---------------------------

.. automodule:: pynao.m_openmx_mat
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_overlap\_am module
---------------------------

.. automodule:: pynao.m_overlap_am
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_overlap\_coo module
----------------------------

.. automodule:: pynao.m_overlap_coo
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_overlap\_lil module
----------------------------

.. automodule:: pynao.m_overlap_lil
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_overlap\_ni module
---------------------------

.. automodule:: pynao.m_overlap_ni
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_pack2den module
------------------------

.. automodule:: pynao.m_pack2den
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_pb\_ae module
----------------------

.. automodule:: pynao.m_pb_ae
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_phonons module
-----------------------

.. automodule:: pynao.m_phonons
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_polariz\_inter\_ave module
-----------------------------------

.. automodule:: pynao.m_polariz_inter_ave
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_prod\_basis\_obsolete module
-------------------------------------

.. automodule:: pynao.m_prod_basis_obsolete
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_prod\_biloc module
---------------------------

.. automodule:: pynao.m_prod_biloc
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_prod\_talman module
----------------------------

.. automodule:: pynao.m_prod_talman
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_report module
----------------------

.. automodule:: pynao.m_report
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_restart module
-----------------------

.. automodule:: pynao.m_restart
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_rf0\_den module
------------------------

.. automodule:: pynao.m_rf0_den
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_rf\_den module
-----------------------

.. automodule:: pynao.m_rf_den
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_rf\_den\_pyscf module
------------------------------

.. automodule:: pynao.m_rf_den_pyscf
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_rsphar module
----------------------

.. automodule:: pynao.m_rsphar
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_rsphar\_libnao module
------------------------------

.. automodule:: pynao.m_rsphar_libnao
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_rsphar\_vec module
---------------------------

.. automodule:: pynao.m_rsphar_vec
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_sbt module
-------------------

.. automodule:: pynao.m_sbt
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_sf2f\_rf module
------------------------

.. automodule:: pynao.m_sf2f_rf
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_siesta2blanko\_csr module
----------------------------------

.. automodule:: pynao.m_siesta2blanko_csr
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_siesta2blanko\_denvec module
-------------------------------------

.. automodule:: pynao.m_siesta2blanko_denvec
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_siesta\_eig module
---------------------------

.. automodule:: pynao.m_siesta_eig
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_siesta\_hsx module
---------------------------

.. automodule:: pynao.m_siesta_hsx
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_siesta\_hsx\_bloch\_mat module
---------------------------------------

.. automodule:: pynao.m_siesta_hsx_bloch_mat
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_siesta\_ion module
---------------------------

.. automodule:: pynao.m_siesta_ion
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_siesta\_ion\_add\_sp2 module
-------------------------------------

.. automodule:: pynao.m_siesta_ion_add_sp2
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_siesta\_ion\_xml module
--------------------------------

.. automodule:: pynao.m_siesta_ion_xml
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_siesta\_units module
-----------------------------

.. automodule:: pynao.m_siesta_units
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_siesta\_utils module
-----------------------------

.. automodule:: pynao.m_siesta_utils
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_siesta\_wfsx module
----------------------------

.. automodule:: pynao.m_siesta_wfsx
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_siesta\_xml module
---------------------------

.. automodule:: pynao.m_siesta_xml
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_siesta\_xml\_print module
----------------------------------

.. automodule:: pynao.m_siesta_xml_print
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_simulation module
--------------------------

.. automodule:: pynao.m_simulation
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_sparsetools module
---------------------------

.. automodule:: pynao.m_sparsetools
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_spline\_diff2 module
-----------------------------

.. automodule:: pynao.m_spline_diff2
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_spline\_interp module
------------------------------

.. automodule:: pynao.m_spline_interp
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_sv\_chain\_data module
-------------------------------

.. automodule:: pynao.m_sv_chain_data
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_sv\_diag module
------------------------

.. automodule:: pynao.m_sv_diag
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_sv\_get\_denmat module
-------------------------------

.. automodule:: pynao.m_sv_get_denmat
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_system\_vars\_deprecated module
----------------------------------------

.. automodule:: pynao.m_system_vars_deprecated
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_system\_vars\_dos module
---------------------------------

.. automodule:: pynao.m_system_vars_dos
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_system\_vars\_gpaw module
----------------------------------

.. automodule:: pynao.m_system_vars_gpaw
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_tddft\_iter\_gpu module
--------------------------------

.. automodule:: pynao.m_tddft_iter_gpu
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_thrj module
--------------------

.. automodule:: pynao.m_thrj
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_tools module
---------------------

.. automodule:: pynao.m_tools
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_valence module
-----------------------

.. automodule:: pynao.m_valence
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_vertex\_loop module
----------------------------

.. automodule:: pynao.m_vertex_loop
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_vhartree\_coo module
-----------------------------

.. automodule:: pynao.m_vhartree_coo
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_vhartree\_pbc module
-----------------------------

.. automodule:: pynao.m_vhartree_pbc
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_vnucele\_coo module
----------------------------

.. automodule:: pynao.m_vnucele_coo
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_vnucele\_coo\_subtract module
--------------------------------------

.. automodule:: pynao.m_vnucele_coo_subtract
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_vxc\_lil module
------------------------

.. automodule:: pynao.m_vxc_lil
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_vxc\_pack module
-------------------------

.. automodule:: pynao.m_vxc_pack
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_x\_zip module
----------------------

.. automodule:: pynao.m_x_zip
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_xc\_scalar\_ni module
------------------------------

.. automodule:: pynao.m_xc_scalar_ni
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_xjl module
-------------------

.. automodule:: pynao.m_xjl
   :members:
   :undoc-members:
   :show-inheritance:

pynao.m\_xjl\_numba module
--------------------------

.. automodule:: pynao.m_xjl_numba
   :members:
   :undoc-members:
   :show-inheritance:

pynao.mesh\_affine\_equ module
------------------------------

.. automodule:: pynao.mesh_affine_equ
   :members:
   :undoc-members:
   :show-inheritance:

pynao.mf module
---------------

.. automodule:: pynao.mf
   :members:
   :undoc-members:
   :show-inheritance:

pynao.nao module
----------------

.. automodule:: pynao.nao
   :members:
   :undoc-members:
   :show-inheritance:

pynao.ndcoo module
------------------

.. automodule:: pynao.ndcoo
   :members:
   :undoc-members:
   :show-inheritance:

pynao.prod\_basis module
------------------------

.. automodule:: pynao.prod_basis
   :members:
   :undoc-members:
   :show-inheritance:

pynao.prod\_log module
----------------------

.. automodule:: pynao.prod_log
   :members:
   :undoc-members:
   :show-inheritance:

pynao.qchem\_inter\_rf module
-----------------------------

.. automodule:: pynao.qchem_inter_rf
   :members:
   :undoc-members:
   :show-inheritance:

pynao.scf module
----------------

.. automodule:: pynao.scf
   :members:
   :undoc-members:
   :show-inheritance:

pynao.scf\_dos module
---------------------

.. automodule:: pynao.scf_dos
   :members:
   :undoc-members:
   :show-inheritance:

pynao.tddft\_iter module
------------------------

.. automodule:: pynao.tddft_iter
   :members:
   :undoc-members:
   :show-inheritance:

pynao.tddft\_iter\_2ord module
------------------------------

.. automodule:: pynao.tddft_iter_2ord
   :members:
   :undoc-members:
   :show-inheritance:

pynao.tddft\_iter\_x\_zip module
--------------------------------

.. automodule:: pynao.tddft_iter_x_zip
   :members:
   :undoc-members:
   :show-inheritance:

pynao.tddft\_tem module
-----------------------

.. automodule:: pynao.tddft_tem
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: pynao
   :members:
   :undoc-members:
   :show-inheritance:
