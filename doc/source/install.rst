.. _install:

============
Installation
============

Requirements
============

In order to install PyNAO, the following packages must be installed

* git
* gcc
* gfortran
* build-essential
* liblapack-dev
* libfftw3-dev
* make
* cmake
* zlib1g-dev
* python3
* python3-pip

PyNAO will need also the following python libraries

* wheel (for installation)
* pytest (for testing)
* pytest-cov (for testing)
* numpy
* scipy>=1.0
* h5py
* numba
* pyscf

Some of the examples make use of the `ASE <https://wiki.fysik.dtu.dk/ase/>`_ package,
therefore its installation is recommended. ASE can be handy to launch Siesta
calculations from Python script and to setup the molecular system.

Installation
============

Architecture file cmake.arch.inc
--------------------------------

In order to compile the C and Fortran codes, you need to edit the file
cmake.arch.inc.
This file contains platform specific instruction such as BLAS and LAPACK libraries
definition (with MKL for example).

You can find examples in the folder ``lib/cmake_user_inc_examples``

Standard installation
---------------------

On Ubuntu and similar OS, PyNAO can easily be instaaled using the Blas and Lapack
libraries. Notice the line where the ``cmake.arch.inc`` is copied.


>>> apt-get update
>>> apt-get install git gcc gfortran build-essential liblapack-dev libfftw3-dev make cmake zlib1g-dev python3 python3-pip
>>> pip3 install -r requirements.txt
>>> export CC=gcc && export FC=gfortran && export CXX=g++
>>> cp lib/cmake_user_inc_examples/cmake.user.inc-gnu lib/cmake.arch.inc
>>> python setup.py bdist_wheel
>>> pip install dist/pynao-0.1-py3-none-any.whl


Installation with Anaconda
--------------------------

For optimal performances, we advise you to use `Anaconda Python <https://www.anaconda.com/>`_
environment. This for three reasons

* First, in Anaconda, NumPy and SciPy libraries are compiled with MKL Blas library (offering in general better performances).
* You can easily use the MKL library included in Anaconda to compile PyNAO (even if it is still possible to use your custom MKL installation).
* At the moment, installing PySCF via pip gives a conflict with the ``libgomp`` library, forbidding PyNAO to be compiled with openmp support. Installing PySCF via conda solves this issues (see PySCF `issue 603 <https://github.com/pyscf/pyscf/issues/603>`_)

In the following, we assume Anaconda is installed under ``${HOME}/anaconda3``.
To properly compile PyNAO with Anaconda, perform the following steps


>>> apt-get update
>>> apt-get install git gcc gfortran build-essential liblapack-dev libfftw3-dev make cmake zlib1g-dev python3 python3-pip
>>> mkdir -p ${HOME}/anaconda3/lib/mkl/lib
>>> ln -s ${HOME}/anaconda3/lib/libmkl* ${HOME}/anaconda3/lib/mkl/lib/
>>> export CC=gcc && export FC=gfortran && export CXX=g++
>>> pip install ase
>>> conda install -c pyscf pyscf

Now, go to ``pynao`` root folder

>>> cp lib/cmake_user_inc_examples/cmake.user.inc-singularity.anaconda.gnu.mkl lib/cmake.arch.inc

Edit the file ``lib/cmake.arch.inc`` to indicate the proper path to MKL library.
You should replace the line ``set(MKLROOT "/opt/conda/lib/mkl/lib")`` by
``set(MKLROOT "${HOME}/anaconda3/lib/mkl/lib")``

then you can finish the installation

>>> python setup.py bdist_wheel
>>> pip install dist/pynao-0.1-py3-none-any.whl


Singularity container
---------------------

We provide in ``share/singularity-img`` a collection of
`Singularity <https://sylabs.io/docs/>`_ recipes that are optimized.
If possible, we advise you to use these recipes to use PyNAO. To build the container

>>> sudo singularity build path2pynao/share/singularity-img/pynao-sing.mkl.gpu pynao-image.img

and to run PyNAO script

>>> singularity run pynao-image.img pynao_script.py


GPU support
-----------

Some part of the code can be accelerated using Nvidia GPUs via the cuBlas library.
For this you need to install the `cupy <https://cupy.dev/>`_ library

>>> pip install cupy

or via `conda`

>>> conda install cupy

You can then pass the argument ``GPU=True`` in order to use GPU for your calculations.
GPU will mainly impact performances for large systems.
