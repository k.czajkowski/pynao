"""
Run DFT+TDDFT calculation on one go.
DFT calulation is first done using Siesta via the ASE wrapper.
Then TDDFT calculations are performed with pynao
"""
import os
import numpy as np

from pynao import tddft_iter
from ase.units import Ry, eV, Ha

# First run DFT calculation with Siesta

dname = os.getcwd() 

report = "tddft_iter_dens_chng_inter_chi0_mv.txt"
tolerances = np.array([1.0e-1, 1.0e-2, 1.0e-3, 1.0e-4, 1.0e-5, 1.0e-6, 1.0e-7,
                       1.0e-8, 1.0e-9, 1.0e-10])
eps = 0.15

for tol in tolerances:

    print("start tolerance: ", tol)
    td = tddft_iter(label='siesta', cd=dname, verbosity=4,
                    iter_broadening=eps/Ha, tol_loc=1e-4, tol_biloc=1e-6, jcutoff=7,
                    level=0, xc_code='LDA,PZ',
                    krylov_options={"tol": tol, "atol": tol})

    # Calculate polarizability
    freq = np.arange(0.0, 5.0, 0.05)/Ha + 1j * td.eps
    pmat = td.comp_polariz_inter_Edir(freq, Eext=np.array([1.0, 0.0, 0.0]))

    np.save("Polarizability_inter_tol{0:.1e}.npy".format(tol), pmat)
    np.save("density_change_prod_basis_inter_optical_tol{0:.1e}.npy".format(tol), td.dn)

    td.write_chi0_mv_timing("report_chi0_mv_step_tol_{0:.1e}.txt".format(tol))

