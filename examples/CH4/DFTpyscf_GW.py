"""
Run DFT+GW calculation on one go.
DFT calulation is first done using Pyscf.
Then GW calculations are performed with pynao
"""
import os
import numpy as np

import ase.io as io
from ase.units import Ry, eV, Ha
from pyscf import gto, scf
from pynao import gw_iter

# First run DFT calculation with Siesta

dname = os.getcwd() 
fname = "CH4.xyz"

# load the molecule geometry
atoms = io.read(fname)

# Convert geometry to string to be loaded into pyscf
atoms_str = ""
for specie, pos in zip(atoms.get_chemical_symbols(), atoms.get_positions()):
    atoms_str += " {0}  {1:.6f}, {2:.6f}, {3:.6f};".format(specie, pos[0],
                                                           pos[1], pos[2])

# Run DFT with pyscf
mol = gto.M(atom=atoms_str, basis='ccpvdz', spin=0)
gto_mf = scf.UHF(mol)
gto_mf.kernel()

# Then run GW calculation with pynao
gw = gw_iter(mf=gto_mf, gto=mol, verbosity=8, rescf=True, niter_max_ev=50,
             tol_ev=1.0e-3, use_initial_guess_ite_solver=True, SCF_kernel_conv_tol=1.0e-8,
             krylov_solver="lgmres", krylov_options={"tol": 1.0e-5, "atol": 1.0e-8},
             GPU=False, dtype=np.float32, gw_xvx_algo="dp_sparse")
gw.kernel_gw_iter()
gw.report()
