from __future__ import print_function, division
import os
import numpy as np
from pynao import tddft_tem
from pynao.m_comp_spatial_distributions import spatial_distribution
from ase.units import Ha, Bohr

dname = os.getcwd()

freq = np.arange(0.0, 25.0, 0.05)/Ha
nao_td  = tddft_tem(label='siesta', cd=dname, verbosity=2, freq=freq)

p_iter = -nao_td.get_spectrum_inter(beam_offset=np.array([0.0, 0.0, 5.0]),
                                    velec=np.array([75.0, 0.0, 0.0])).imag
data = np.array([nao_td.freq.real*Ha, p_iter])
np.savetxt('CH4.tddft_tem_lda.omega.inter.pav.txt', data.T, fmt=['%f','%f'])
np.save('CH4.dn_tem_lda.omega.inter.npy', nao_td.dn)

box = np.array([[-10.0, 10.0],
                [-10.0, 10.0],
                [-10.0, 10.0]])/Bohr
dr = np.array([0.5, 0.5, 0.5])/Bohr

# initialize spatial calculations
# see filepynao.m_comp_spatial_distributions for some description of the inputs
# be aware that the density change in product basis, nao_td.dn could be loaded
# from file, allowing you to change the spatial density distribution parameters
# without redoing the full calculation
spd = spatial_distribution(nao_td.dn, freq, box, dr=dr, label="siesta",
                           excitation="electron")

# compute spatial density change distribution at a specific frequency
# In general you try to plot it at an excitation seen in the spectra
w0 = 10.95/Ha
spd.get_spatial_density(w0)

np.save("dn_tem_spatial.npy", spd.dn_spatial)

# compute Efield
Efield = spd.comp_induce_field()
np.save("Efield_tem_spatial.npy", Efield)

# compute potential
pot = spd.comp_induce_potential()
np.save("pot_tem_spatial.npy", pot)

# compute intensity
intensity = spd.comp_intensity_Efield(Efield)
np.save("Efield_intensity_tem_spatial.npy", intensity)
