from __future__ import print_function, division
import os,unittest,numpy as np
from pynao import mf as mf_c
from pynao.m_siesta_units import siesta_conv_coefficients
from pynao.m_overlap_ni import overlap_ni

class KnowValues(unittest.TestCase):

    def test_0073_vna_vnl_N2(self):
        """
        Test the Ena energy and indirectly VNA matrix elements
        """
        dname = os.path.dirname(os.path.abspath(__file__))
        mf = mf_c(label='n2', cd=dname)
        vna = mf.vna_coo(level=3).toarray()
        rdm = mf.make_rdm1()[0,0,:,:,0]
        Ena = siesta_conv_coefficients["ha2ev"]*(-0.5)*(vna*rdm).sum()
        # previous value for Ena: 133.24212864149359
        # changed the 2020-08-11 for merge request 41
        # https://gitlab.com/mbarbry/pynao/-/merge_requests/41
        # siesta: Ena     =       133.196299
        self.assertAlmostEqual(Ena, 133.2421212907487)

        vnl = mf.vnl_coo().toarray()
        Enl = siesta_conv_coefficients["ha2ev"]*(vnl*rdm).sum()
        # previous value for Enl: -61.604522776730128
        # changed the 2020-08-11 for merge request 41
        # https://gitlab.com/mbarbry/pynao/-/merge_requests/41
        # siesta: Enl     =       -61.601204
        self.assertAlmostEqual(Enl, -61.604520792944875)

if __name__ == "__main__":
    unittest.main()
