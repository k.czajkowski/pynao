from __future__ import print_function, division
import os,unittest,numpy as np
from pynao import tddft_iter
from pyscf import gto, scf, tddft
from pyscf.data.nist import HARTREE2EV

dname = os.path.dirname(os.path.abspath(__file__))

class KnowValues(unittest.TestCase):

    def test_135_h2o_uhf_rpa_pb(self):
        """
        This
        """
        mol = gto.M(verbose=1,atom='O 0 0 0; H 0 0.489 1.074; H 0 0.489 -1.074',
                    basis='cc-pvdz')
        gto_mf = scf.UHF(mol)
        gto_mf.kernel()
        nao_mf = tddft_iter(gto=mol, mf=gto_mf, tol_loc=1e-5, tol_biloc=1e-7, xc_code="RPA")
        comega = np.arange(0.0, 2.0, 0.01) + 1j*0.03
        pnonin = -nao_mf.comp_polariz_inter_ave(comega).imag
        data = np.array([comega.real*HARTREE2EV, pnonin])
        np.savetxt('test_135_h2o_uhf_rpa_pb.txt', data.T, fmt=['%f','%f'])
        data_ref = np.loadtxt(dname + '/test_135_h2o_uhf_rpa_pb.txt-ref').T
        self.assertTrue(np.allclose(data_ref, data, atol=1e-6, rtol=1e-3))
 
if __name__ == "__main__":
    unittest.main()
