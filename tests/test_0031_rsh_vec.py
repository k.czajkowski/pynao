from __future__ import print_function, division
import os,unittest,numpy as np
from pynao.m_rsphar_libnao import rsphar_vec as rsphar_vec_libnao

class KnowValues(unittest.TestCase):

    def test_rsh_vec(self):
        """
        Compute real spherical harmonics via a vectorized algorithm
        """
        from pynao.m_rsphar_libnao import rsphar_exp_vec as rsphar_exp_vec_libnao
        from pynao.m_rsphar_vec import rsphar_vec as rsphar_vec_python

        ll = [0,1,2,3,4]
        crds = np.random.rand(20000, 3)
        for lmax in ll:
            rsh1 = rsphar_exp_vec_libnao(crds.T, lmax)
            rsh2 = rsphar_vec_libnao(crds, lmax)
          
if __name__ == "__main__":
    unittest.main()
