from __future__ import print_function, division
import unittest
from pyscf import gto
from pynao import nao
from pynao.prod_log import prod_log as prod_log_c

mol = gto.M(
    verbose = 1,
    atom = '''
        O     0    0        0
        H     0    -0.757   0.587
        H     0    0.757    0.587''',
    basis = 'cc-pvdz',
)

class KnowValues(unittest.TestCase):

    def test_gto2sv_df(self):
        """
        Test import of density-fitting Gaussian functions ... hm
        """

        from pyscf import scf
    
        mf = scf.density_fit(scf.RHF(mol))
        self.assertAlmostEqual(mf.scf(), -76.025936299702536, 2)
        sv = nao(gto=mol)
        prod_log = prod_log_c(auxmol=mf.with_df.auxmol, nao=sv)
        self.assertEqual(prod_log.rr[0], sv.ao_log.rr[0])
        self.assertEqual(prod_log.pp[0], sv.ao_log.pp[0])
        self.assertEqual(prod_log.nspecies, sv.ao_log.nspecies)
        for a,b in zip(prod_log.sp2charge, sv.ao_log.sp2charge):
            self.assertEqual(a, b)

    def test_gto2sv_prod_log(self):
        """
        Test what ?
        """
    
        sv = nao(gto=mol)
        prod_log = prod_log_c(ao_log=sv.ao_log, tol_loc=1e-4)
        mae,mxe,lll=prod_log.overlap_check()
        self.assertTrue(all(lll))
        self.assertEqual(prod_log.nspecies, 2)
        self.assertEqual(prod_log.sp2nmult[0], 7)
        self.assertEqual(prod_log.sp2nmult[1], 20)
        self.assertEqual(prod_log.sp2norbs[0], 15)
        self.assertEqual(prod_log.sp2norbs[1], 70)

if __name__ == "__main__":
    unittest.main()

