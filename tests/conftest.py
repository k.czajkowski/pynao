import pytest
from ase.utils import workdir

#@pytest.fixture(autouse=True)
#def goto_tempdir(tmp_path):
#    with workdir(tmp_path):
#        yield


@pytest.fixture
def tmp_dir(tmp_path):
    # This is simply an alias for pytest's own tmp_path.
    # Although it shadows pytest's built-in tmp_dir fixture.
    return tmp_path


