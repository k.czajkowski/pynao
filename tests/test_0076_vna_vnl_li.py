from __future__ import print_function, division
import unittest
from pynao import nao as nao_c
from pynao.m_siesta_units import siesta_conv_coefficients

class KnowValues(unittest.TestCase):

    def test_0076_vna_vnl_Li(self):
        """
        Test of the energy decomposition
        """
        import os
        dname = os.path.dirname(os.path.abspath(__file__))
        nao = nao_c(label='li', cd=dname)
        n = nao.norbs
        dm  = nao.make_rdm1().reshape((n,n))
        vna = nao.vna_coo().toarray()

        Ena = (vna*dm).sum()*(-0.5)*siesta_conv_coefficients["ha2ev"]
        # previous value for Ena: 0.9790944563943327
        # changed the 2020-08-11 for merge request 41
        # https://gitlab.com/mbarbry/pynao/-/merge_requests/41
        # siesta: Ena     =         4.136159
        self.assertAlmostEqual(Ena, 0.9791425199864835)

        vnl = nao.vnl_coo().toarray()
        Enl = (vnl*dm).sum()*siesta_conv_coefficients["ha2ev"]
        # previous value for Enl: 1.3121153971023498
        # changed the 2020-08-11 for merge request 41
        # https://gitlab.com/mbarbry/pynao/-/merge_requests/41
        # siesta: Enl     =         1.312064
        self.assertAlmostEqual(Enl, 1.3121798086398468)

        vkin = -0.5*nao.laplace_coo().toarray() # Why not -0.5*Laplace ?
        Ekin = (vkin*dm).sum()*siesta_conv_coefficients["ha2ev"]
        # previous value for Ena: 2.6284589375945457
        # changed the 2020-08-11 for merge request 41
        # https://gitlab.com/mbarbry/pynao/-/merge_requests/41
        # siesta: Ekin     =       2.628343
        self.assertAlmostEqual(Ekin, 2.6285879682284303)

# siesta: Ebs     =        -2.302303
# siesta: Eions   =         9.635204
# siesta: Ena     =         4.136159
# siesta: Ekin    =         2.628343
# siesta: Enl     =         1.312064
# siesta: DEna    =         0.006680
# siesta: DUscf   =         0.000010
# siesta: DUext   =         0.000000
# siesta: Exc     =        -4.225864

if __name__ == "__main__":
    unittest.main()
