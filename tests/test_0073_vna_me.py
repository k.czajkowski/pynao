from __future__ import print_function, division
import os,unittest,numpy as np
from pynao import mf as mf_c
from pynao.m_siesta_units import siesta_conv_coefficients

class KnowValues(unittest.TestCase):

    def test_vneutral_atom_matrix_elements(self):
        """
        reSCF then G0W0
        """

        dname = os.path.dirname(os.path.abspath(__file__))
        mf = mf_c(label='n2', cd=dname)
        vna = mf.vna_coo(level=1).toarray()
        rdm = mf.make_rdm1()[0,0,:,:,0]
        Ena = siesta_conv_coefficients["ha2ev"]*(-0.5)*(vna*rdm).sum()

        # previous value for Ena: 133.24203392039948
        # changed the 2020-08-11 for merge request 41
        # https://gitlab.com/mbarbry/pynao/-/merge_requests/41
        self.assertAlmostEqual(Ena, 133.24195811669125)

if __name__ == "__main__":
    unittest.main()
