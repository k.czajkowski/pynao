from __future__ import print_function, division
import os,unittest,numpy as np
from pynao import tddft_iter
from pyscf import gto, dft, scf, tddft
from pyscf.data.nist import HARTREE2EV

dname = os.path.dirname(os.path.abspath(__file__))

class KnowValues(unittest.TestCase):

    def test_144_h2b_uks_pz_pb(self):
        """
        This
        """
        mol = gto.M(verbose=1, atom='B 0 0 0; H 0 0.489 1.074; H 0 0.489 -1.074',
                    basis='cc-pvdz', spin=3)
        gto_mf = dft.UKS(mol)
        gto_mf.kernel()
        vhf = gto_mf.get_veff(mol=mol)
        nao_mf = tddft_iter(gto=mol, mf=gto_mf, tol_loc=1e-5, tol_biloc=1e-7)
        comega = np.arange(0.0, 2.0, 0.01) + 1j*0.03
        polave = -nao_mf.polariz_inter_ave(comega).imag
        data = np.array([comega.real*HARTREE2EV, polave])
        np.savetxt('test_144_h2b_uks_pz_pb.txt', data.T, fmt=['%f','%f'])
        data_ref = np.loadtxt(dname + '/test_144_h2b_uks_pz_pb.txt-ref').T
        self.assertTrue(np.allclose(data_ref, data, atol=1e-6, rtol=1e-3))

if __name__ == "__main__":
    unittest.main()
