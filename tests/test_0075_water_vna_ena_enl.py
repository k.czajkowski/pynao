from __future__ import print_function, division
import unittest
from pynao import nao as nao_c, scf as scf_c
from pynao.m_siesta_units import siesta_conv_coefficients

class KnowValues(unittest.TestCase):

    def test_0075_vna_vnl_H2O(self):
        """
        Test of the energy decomposition
        """
        import os
        dname = os.path.dirname(os.path.abspath(__file__))
        nao = nao_c(label='water', cd=dname)
        n = nao.norbs
        dm  = nao.make_rdm1().reshape((n,n))
        vna = nao.vna_coo().toarray()

        Ena = (vna*dm).sum()*(-0.5)*siesta_conv_coefficients["ha2ev"]
        # previous value for Ena: 132.50585488810401
        # changed the 2020-08-11 for merge request 41
        # https://gitlab.com/mbarbry/pynao/-/merge_requests/41
        # siesta: Ena     =       175.007584
        self.assertAlmostEqual(Ena, 132.50584978125858)

        vnl = nao.vnl_coo().toarray()
        Enl = (vnl*dm).sum()*siesta_conv_coefficients["ha2ev"]
        # previous value for Enl: -62.176213752828893)
        # changed the 2020-08-11 for merge request 41
        # https://gitlab.com/mbarbry/pynao/-/merge_requests/41
        # siesta: Enl     =       -62.176200
        self.assertAlmostEqual(Enl, -62.17621274618229)

        vkin = -0.5*nao.laplace_coo().toarray() # Why not -0.5*Laplace ?
        Ekin = (vkin*dm).sum()*siesta_conv_coefficients["ha2ev"]
        # previous value for Ekin: 351.76677461783862
        # changed the 2020-08-11 for merge request 41
        # https://gitlab.com/mbarbry/pynao/-/merge_requests/41
        # siesta: Ekin     =       351.769106
        self.assertAlmostEqual(Ekin, 351.7667693084848)

# siesta: Ebs     =      -103.137894
# siesta: Eions   =       815.854478
# siesta: Ena     =       175.007584
# siesta: Ekin    =       351.769106
# siesta: Enl     =       -62.176200
# siesta: DEna    =        -2.594518
# siesta: DUscf   =         0.749718
# siesta: DUext   =         0.000000

if __name__ == "__main__":
    unittest.main()
