from __future__ import print_function, division
import unittest
from pynao import nao 
from pynao.m_gauleg import gauss_legendre 
from pyscf import dft
import numpy as np

class KnowValues(unittest.TestCase):

    def test_bilocal(self):
        """
        Build 3d integration scheme for two centers.
        """
        
        sv=nao(xyz_list=[ [8, [0.0, 0.0, 0.0]], [1, [1.0, 1.0, 1.0] ]])
        atom2rcut=np.array([5.0, 4.0])
        grids = dft.gen_grid.Grids(sv)
        # precision as implemented in pyscf
        grids.level = 2
        grids.radi_method = gauss_legendre
        grids.build(atom2rcut=atom2rcut)
        self.assertEqual(len(grids.weights), 20648)

    def test_one_center(self):
        """
        Build 3d integration coordinates and weights for just one center.
        """
        
        sv=nao(xyz_list=[ [8, [0.0, 0.0, 0.0]]])
        atom2rcut=np.array([5.0])
        g = dft.gen_grid.Grids(sv)
        # precision as implemented in pyscf
        g.level = 1
        g.radi_method = gauss_legendre
        g.build(atom2rcut=atom2rcut)

        self.assertAlmostEqual(max(  np.linalg.norm(g.coords, axis=1)  ),
                               4.9955942742763986)
        self.assertAlmostEqual(g.weights.sum(), 4.0 *np.pi*5.0**3 / 3.0)
        self.assertEqual(len(g.weights), 6248)
    

if __name__ == "__main__":
    unittest.main()
