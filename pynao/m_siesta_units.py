"""
Units conversion for Siesta outputs

Used the definition from Scipy
https://docs.scipy.org/doc/scipy/reference/constants.html
"""
import scipy.constants as cte

def get_conversion_factors(definition_year=2014):
    """
    Possibility to used different definition of the physicale constants as
    defined by the CODATA

    By default, PyNAO used the definitions from the year 2014 (even if they are
    not the latest values). This is because the tests for PyNAO were set up
    with this definition. Changing the year may make some tests fails.

    References
    ----------
    Theoretical and experimental publications relevant to the fundamental constants
    and closely related precision measurements published since the mid 1980s, but
    also including many older papers of particular interest, some of which date
    back to the 1800s. To search bibliography visit

    https://physics.nist.gov/cuu/Constants/
    """

    if definition_year != 2014:
        mess = """
        The PyNAO tests are set up with the physical constant definition of the
        year 2014. Changing the year will makes some tests failing.
        """
        warnings.warn(mess)

    if definition_year == 2002:
        phy_constant = cte.codata._physical_constants_2002
    elif definition_year == 2006:
        phy_constant = cte.codata._physical_constants_2006
    elif definition_year == 2010:
        phy_constant = cte.codata._physical_constants_2010
    elif definition_year == 2014:
        phy_constant = cte.codata._physical_constants_2014
    elif definition_year == 2018:
        phy_constant = cte.codata._physical_constants_2018
    else:
        raise ValueError("unknow year definition")

    siesta_conv_coefficients = {
            "ha2ev": phy_constant["Hartree energy in eV"][0],
            "ry2ha": 0.5,
            "bohr2ang": phy_constant["Bohr radius"][0]*1.0e10,
            }
    # siesta seems to use fundamental constantes of poor accuracy ??
    siesta_conv_coefficients["ha2ev_siesta_corr"] = \
            siesta_conv_coefficients["ha2ev"]/27.21163

    # Get the inverse relations
    for key in ["ha2ev", "ry2ha", "bohr2ang"]:
        val = siesta_conv_coefficients[key]
        units = key.split("2")
        new_key = "{}2{}".format(units[1], units[0])
        siesta_conv_coefficients[new_key] = 1.0/val

    return siesta_conv_coefficients

siesta_conv_coefficients = get_conversion_factors()
