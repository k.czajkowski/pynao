from __future__ import print_function, division
from copy import copy
import numpy as np

from ase.units import Ha

from pynao.chi0_matvec import chi0_matvec
from pynao.m_chi0_noxv import chi0_mv_gpu, chi0_mv
from pynao.m_kernel_utils import kernel_initialization

from scipy.sparse import csr_matrix, coo_matrix
from scipy.linalg import blas
from pynao.m_sparsetools import csr_matvec, csc_matvec, csc_matvecs
from tqdm import tqdm
import matplotlib.pyplot as plt

class tddft_iter(chi0_matvec):
    """ 
    Iterative TDDFT a la PK, DF, OC JCTC

    Input Parameters:
    -----------------
        kw: keywords arguments:
            * tmp_fname (string, default None): temporary file to save polarizability
                            at each frequency. Can be a life saver for large systems.
    """

    def __init__(self, **kw):

        self.load_kernel = load_kernel = kw['load_kernel'] if 'load_kernel' in kw else False

        chi0_matvec.__init__(self, **kw)

        self.xc_code_mf = copy(self.xc_code)
        if "xc_code" in kw.keys():
            self.xc_code = kw['xc_code']
        else:
            kw['xc_code'] = self.xc_code

        if self.GPU:
            self.chi0_mv = chi0_mv_gpu
        else:
            self.chi0_mv = chi0_mv

        if not hasattr(self, 'pb'):
          print(__name__, 'no pb?')
          print(__name__, kw.keys())
          raise ValueError("product basis not initialized?")

        self.kernel, self.kernel_dim, self.ss2kernel = \
                kernel_initialization(self, **kw)

        if self.verbosity > 0:
            print(__name__,'\t====> self.xc_code:', self.xc_code)

    def comp_fxc_lil(self, **kw): 
        """
        Computes the sparse version of the TDDFT interaction kernel
        """
        from pynao.m_vxc_lil import vxc_lil
        return vxc_lil(self, deriv=2, ao_log=self.pb.prod_log, **kw)

    def comp_fxc_pack(self, **kw): 
        """
        Computes the packed version of the TDDFT interaction kernel
        """
        from pynao.m_vxc_pack import vxc_pack
        return vxc_pack(self, deriv=2, ao_log=self.pb.prod_log, **kw)

    def comp_veff(self, vext, comega=1j*0.0, x0=None):
        """
        This computes an effective field (scalar potential) given the external
        scalar potential.

        Solves
            
            Ax = b

        with
            * b the external potential delta V_ext
            * x the effective potential delta V_eff
            * A = (1 - K_{Hxc}Chi_0)
        """
        from scipy.sparse.linalg import LinearOperator

        self.matvec_ncalls = 0
        nsp = self.nspin*self.nprod
        assert len(vext) == nsp, "{} {}".format(len(vext), nsp)
        self.comega_current = comega
        veff_op = LinearOperator((nsp, nsp), matvec=self.vext2veff_matvec,
                                 dtype=self.dtypeComplex)

        # right hand side of the equation
        rhs = np.require(vext, dtype=self.dtypeComplex, requirements='C')

        #print("shape: ", veff_op.shape, rhs.shape)
        # Solves Ax = b
        resgm, info = self.krylov_solver(veff_op, rhs, x0=x0, **self.krylov_options)

        if info != 0:
            print("LGMRES Warning: info = {0}".format(info))

        return resgm

    def vext2veff_matvec(self, vin):
        dn0 = self.apply_rf0(vin, self.comega_current, self.chi0_mv)
        vcre, vcim = self.apply_kernel(dn0)
        return vin - (vcre + 1.0j*vcim)

    def vext2veff_matvec2(self, vin):
        dn0 = self.apply_rf0(vin, self.comega_current, self.chi0_mv)
        vcre,vcim = self.apply_kernel(dn0)
        return 1 - (vin - (vcre + 1.0j*vcim))

    def apply_kernel(self, dn):
        if self.nspin == 1:
            return self.apply_kernel_nspin1(dn)
        elif self.nspin == 2:
            return self.apply_kernel_nspin2(dn)

    def apply_kernel_nspin1(self, dn):

        daux  = np.zeros(self.nprod, dtype=self.dtype)
        daux[:] = np.require(dn.real, dtype=self.dtype, requirements=["A","O"])
        vcre = self.spmv(self.nprod, 1.0, self.kernel, daux)

        daux[:] = np.require(dn.imag, dtype=self.dtype, requirements=["A","O"])
        vcim = self.spmv(self.nprod, 1.0, self.kernel, daux)
        return vcre,vcim

    def apply_kernel_nspin2(self, dn):

        vcre = np.zeros((2,self.nspin,self.nprod), dtype=self.dtype)
        daux = np.zeros((self.nprod), dtype=self.dtype)
        s2dn = dn.reshape((self.nspin,self.nprod))

        for s in range(self.nspin):
            for t in range(self.nspin):
                for ireim,sreim in enumerate(('real', 'imag')):
                    daux[:] = np.require(getattr(s2dn[t], sreim),
                                         dtype=self.dtype, requirements=["A","O"])
                    vcre[ireim,s] += self.spmv(self.nprod, 1.0, self.ss2kernel[s][t], daux)

        return vcre[0].reshape(-1), vcre[1].reshape(-1)

    def comp_polariz_nonin_xx(self, comegas, tmp_fname=None):
        """
        Compute the non-interacting polarizability along the xx direction
        """
        self.dn0, self.p0_mat = \
                self.comp_dens_along_Eext(comegas,
                                          Eext=np.array([1.0, 0.0, 0.0]),
                                          tmp_fname=tmp_fname,
                                          inter=False)
        return self.p0_mat[0, 0, :]

    def comp_polariz_inter_xx(self, comegas, tmp_fname=None):
        """
        Compute the interacting polarizability along the xx direction
        """
        self.dn, self.p_mat = \
                self.comp_dens_along_Eext(comegas,
                                          Eext=np.array([1.0, 0.0, 0.0]),
                                          tmp_fname=tmp_fname,
                                          inter=True)
        return self.p_mat[0, 0, :]

    def comp_polariz_inter_xx_atom_split(self, comegas):
        """  Compute the interacting polarizability along the xx direction split into contributions from individual atoms."""
        aw2pxx = np.zeros((self.natoms, comegas.shape[0]), dtype=self.dtypeComplex)

        vext = np.transpose(self.moms1)
        nww, eV = len(comegas), 27.2114
        HARTREE2EV = eV
        for iw, comega in enumerate(comegas):
            if self.verbosity>0: print(iw, nww, comega.real*HARTREE2EV)
            veff = self.comp_veff(vext[0], comega)
            dn = self.apply_rf0(veff, comega, self.chi0_mv)
            for ia in range(self.natoms):
                dn_atom = np.zeros(self.nprod, dtype=complex)
                dn_atom[self.pb.c2s[ia]:self.pb.c2s[ia+1]] = dn[self.pb.c2s[ia]:self.pb.c2s[ia+1]]
                aw2pxx[ia, iw] = np.dot(dn_atom, vext[0])
        return aw2pxx

    def comp_polariz_nonin_ave(self, comegas, tmp_fname=None):
        """
        Compute average interacting polarizability
        """

        self.dn0, self.p0_mat = \
                self.comp_dens_along_Eext(comegas,
                                          Eext=np.array([1.0, 1.0, 1.0]),
                                          tmp_fname=tmp_fname,
                                          inter=False)

        Pavg = np.zeros((self.p0_mat.shape[2]), dtype=self.dtypeComplex)
        for i in range(3):
            Pavg[:] += self.p0_mat[i, i, :]

        return Pavg/3

    def comp_polariz_inter_ave(self, comegas, tmp_fname=None):
        """
        Compute average interacting polarizability
        """

        self.dn, self.p_mat = \
                self.comp_dens_along_Eext(comegas,
                                          Eext=np.array([1.0, 1.0, 1.0]),
                                          tmp_fname=tmp_fname,
                                          inter=True)

        Pavg = np.zeros((self.p_mat.shape[2]), dtype=self.dtypeComplex)
        for i in range(3):
            Pavg[:] += self.p_mat[i, i, :]

        return Pavg/3
    polariz_inter_ave = comp_polariz_inter_ave

    def comp_polariz_nonin_Edir(self, comegas, Eext=np.array([1.0, 1.0, 1.0]),
                                tmp_fname=None):
        """
        Compute average interacting polarizability
        """

        self.dn0, self.p0_mat = \
                self.comp_dens_along_Eext(comegas, Eext=Eext,
                                          tmp_fname=tmp_fname,
                                          inter=False)

        return self.p0_mat

    def comp_polariz_inter_Edir(self, comegas, Eext=np.array([1.0, 1.0, 1.0]),
                                tmp_fname=None):
        """
        Compute average interacting polarizability
        """

        self.dn, self.p_mat = \
                self.comp_dens_along_Eext(comegas, Eext=Eext,
                                          tmp_fname=tmp_fname,
                                          inter=True)

        return self.p_mat

    def comp_polariz_KS_split(self, comega, emax = 10):
        vext = np.transpose(self.moms1)[0]
        dvin = self.comp_veff(vext, comega)

        vs, nf = self.vstart[0], self.nfermi[0]
        pol = np.zeros((self.nspin, self.ksn2e[0, 0, :nf].shape[0], self.ksn2e[0, 0, vs:].shape[0])) + 0.0 * 1j
        emax = emax / 27.2111  # 10 eV jako emax

        sp2v = dvin.reshape((self.nspin, self.nprod))

        for s in range(self.nspin):
            vdp = csr_matvec(self.cc_da, sp2v[s].real)  # real part
            sab = (vdp * self.v_dab).reshape((self.norbs, self.norbs))

            nb2v = self.gemm(1.0, self.xocc[s], sab)
            nm2v_re = self.gemm(1.0, nb2v, self.xvrt[s].T)

            vdp = csr_matvec(self.cc_da, sp2v[s].imag)  # imaginary
            sab = (vdp * self.v_dab).reshape((self.norbs, self.norbs))

            nb2v = self.gemm(1.0, self.xocc[s], sab)
            nm2v_im = self.gemm(1.0, nb2v, self.xvrt[s].T)

            vs, nf = self.vstart[s], self.nfermi[s]

            self.div_numba(self.ksn2e[0, s], self.ksn2f[0, s], nf, vs, comega, nm2v_re, nm2v_im)

            nm2v_re_bu = nm2v_re + 0.0
            nm2v_im_bu = nm2v_im + 0.0
            nm2v_re = 0 * nm2v_re
            nm2v_im = 0 * nm2v_im
            pbar = tqdm(total=self.ksn2e[0, 0, :nf].shape[0])
            for n, (en, fn) in enumerate(zip(self.ksn2e[0, s, :nf], self.ksn2f[0, s, :nf])):
                for m, (em, fm) in enumerate(zip(self.ksn2e[0, s, vs:], self.ksn2f[0, s, vs:])):
                    if not (nm2v_re_bu[n, m] == 0.0 and nm2v_im_bu[n, m] == 0.0):
                        if not (abs(en - self.fermi_energy) > emax or abs(em - self.fermi_energy) > emax):
                            nm2v_re[n, m] = nm2v_re_bu[n, m]
                            nm2v_im[n, m] = nm2v_im_bu[n, m]
                            nb2v = self.gemm(1.0, nm2v_re, self.xvrt[s])  # real part
                            ab2v = self.gemm(1.0, self.xocc[s].T, nb2v).reshape(self.norbs * self.norbs)
                            vdp = csr_matvec(self.v_dab, ab2v)
                            chi0_re = vdp * self.cc_da

                            nb2v = self.gemm(1.0, nm2v_im, self.xvrt[s])  # imag part
                            ab2v = self.gemm(1.0, self.xocc[s].T, nb2v).reshape(self.norbs * self.norbs)
                            vdp = csr_matvec(self.v_dab, ab2v)
                            chi0_im = vdp * self.cc_da

                            pol_re = blas.ddot(vext, chi0_re)
                            pol_im = blas.ddot(vext, chi0_im)

                            pol[s, n, m] = pol_re + 1j * pol_im

                            nm2v_re[n, m] = 0.0
                            nm2v_im[n, m] = 0.0
                pbar.update(1)
        pbar.close()
        return pol

    def comp_transition_map(self, comega, e_max=10, N=300):
        pxx = np.squeeze(self.comp_polariz_KS_split(comega, e_max).imag)
        vs, nf = self.vstart[0], self.nfermi[0]
        ef=self.fermi_energy*Ha
        en_i=self.ksn2e[0,0,:nf]*Ha
        en_a=self.ksn2e[0,0,vs:]*Ha

        gauss = lambda x,y,x0,y0,sig : np.exp(-((x-x0)**2+(y-y0)**2)/(2*sig**2))/(2*np.pi*sig**2)
        ptot = np.sum(pxx)
        
        vi=np.linspace(-8,0,N)+ef
        va=np.linspace(0,8,N)+ef
        I, A=np.meshgrid(vi,va)

        map0=pxx/ptot
        sig=0.27
        tot=0
        ii=-1
        for a in en_a:
            ii=ii+1
            jj=-1
            for i in en_i:
                jj=jj+1
                tot=tot+gauss(I,A,i,a,sig)*map0[jj,ii]
        vi-=ef
        va-=ef
        return vi,va,tot

    def draw_transition_map(self,vi,va,tot):
        fig = plt.figure()
        mt = np.max(np.abs(tot)) 
        extents = lambda f : [f[0] - (f[1] - f[0])/2, f[-1] + (f[1] - f[0])/2]
        plt.set_cmap('bwr')
        plt.imshow(tot, aspect='auto', interpolation='none',
                   extent=extents(vi) + extents(va), origin='lower')
        plt.clim([-mt,mt])
        plt.colorbar()