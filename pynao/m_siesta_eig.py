from pynao.m_siesta_ev2ha import siesta_ev2ha
from pynao.m_siesta_units import siesta_conv_coefficients

def siesta_eig(label='siesta'):
    with open(label+'.EIG', 'r') as f:
      f.seek(0)
      Fermi_energy_eV = float(f.readline())
      Fermi_energy_Ha = Fermi_energy_eV*siesta_conv_coefficients["ev2ha"]
  return Fermi_energy_Ha
